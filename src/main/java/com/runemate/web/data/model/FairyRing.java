package com.runemate.web.data.model;

import com.runemate.web.requirement.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class FairyRing {

    @NonNull String code;
    @NonNull Coordinate position;
    @NonNull Requirement requirement;

}
