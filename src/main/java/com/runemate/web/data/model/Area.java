package com.runemate.web.data.model;

import com.runemate.web.vertex.*;
import lombok.*;

@AllArgsConstructor
public class Area {

    private final int x, y, width, height;

    public boolean contains(@NonNull final Vertex vertex) {
        return vertex.x() >= x && vertex.y() >= y && vertex.x() < x + width && vertex.y() < y + height;
    }

}
