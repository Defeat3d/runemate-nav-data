package com.runemate.web.data.model;

import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class GameObjectTransport extends FixedTransport {

    @NonNull Coordinate objectPosition;
    @NonNull String objectName;
    @NonNull String objectAction;

}
