package com.runemate.web.data.model;

import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class DialogNpcTransport extends NpcTransport {

    @NonNull Pattern dialogPattern;

}
