package com.runemate.web.data.model;

import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class BasicItemTransport extends DynamicTransport {

    @NonNull Pattern itemName;
    @NonNull Pattern itemAction;
    @NonNull Origin origin;

    public enum Origin {
        EQUIPMENT,
        INVENTORY
    }
}
