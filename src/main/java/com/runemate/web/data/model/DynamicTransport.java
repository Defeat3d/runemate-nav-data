package com.runemate.web.data.model;

import com.runemate.web.requirement.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class DynamicTransport {

    @NonNull String description;
    @NonNull Coordinate destination;
    @NonNull Requirement requirement;

}
