package com.runemate.web.data.model;

import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class NpcTransport extends FixedTransport {

    @NonNull String npcName;
    @NonNull String npcAction;
    float wanderRadius;

}
