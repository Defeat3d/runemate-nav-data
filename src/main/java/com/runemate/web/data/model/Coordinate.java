package com.runemate.web.data.model;

import com.runemate.web.vertex.*;
import lombok.*;

@Value
public class Coordinate {

    int x, y, plane;

    public Vertex asVertex() {
        return new VertexLiteral(x, y, plane);
    }
}
