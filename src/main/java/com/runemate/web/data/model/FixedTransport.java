package com.runemate.web.data.model;

import com.runemate.web.requirement.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
public class FixedTransport {

    @NonNull String description;
    @NonNull Coordinate source;
    @NonNull Coordinate destination;
    @NonNull Requirement requirement;

}
