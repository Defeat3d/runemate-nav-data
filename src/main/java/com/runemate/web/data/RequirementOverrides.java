package com.runemate.web.data;

import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.vertex.*;
import lombok.experimental.*;

/**
 * Many underworld objects are added automatically by the graph generator, but it has no way of determining if the object has
 * any requirements to use.
 * <p>
 * This class aims to remedy that by providing a means to pre-define requirements for specific object positions.
 */
@UtilityClass
public class RequirementOverrides {

    private final Area DWARVEN_MINE_OVERWORLD = new Area(3017, 3337, 4, 4);

    public static Requirement override(final Vertex vertex) {
        if (DWARVEN_MINE_OVERWORLD.contains(vertex)) {
            return SkillRequirement.Skill.MINING.required(60);
        }

        return Requirements.none();
    }

}
