package com.runemate.web.data;

import com.runemate.web.data.model.*;
import java.util.*;
import lombok.experimental.*;

/**
 * Collects the various StaticTransports
 */
@UtilityClass
public class FixedTransports {

    public Collection<FixedTransport> load() {
        final List<FixedTransport> transports = new ArrayList<>();

        transports.addAll(AgilityShortcuts.getAll());
        transports.addAll(CharterShips.getAll());
        transports.addAll(DungeonEntrances.getAll());

        return transports;
    }

}
