package com.runemate.web.data;

import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import java.util.*;
import lombok.experimental.*;

@UtilityClass
public class SpiritTrees {

    public Collection<SpiritTree> getAll() {
        final var req = new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE);
        return Arrays.asList(
            SpiritTree.builder()
                .location("Tree gnome Village")
                .position(new Coordinate(2542, 3170, 0))
                .requirement(req)
                .build(),
            SpiritTree.builder()
                .location("Gnome Stronghold")
                .position(new Coordinate(2461, 3444, 0))
                .requirement(req.and(new QuestRequirement("THE_GRAND_TREE", QuestRequirement.State.COMPLETE)))
                .build(),
            SpiritTree.builder()
                .location("Battlefield of Khazard")
                .position(new Coordinate(2555, 3259, 0))
                .requirement(req)
                .build(),
            SpiritTree.builder()
                .location("Grand Exchange")
                .position(new Coordinate(3185, 3508, 0))
                .requirement(req)
                .build(),
            SpiritTree.builder()
                .location("Feldip Hills")
                .position(new Coordinate(2488, 2850, 0))
                .requirement(req)
                .build()
        );
    }
}
