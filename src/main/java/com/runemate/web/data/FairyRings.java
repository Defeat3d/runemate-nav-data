package com.runemate.web.data;

import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;

@UtilityClass
public class FairyRings {

    public Collection<FairyRing> getAll() {
        return Arrays.asList(
            build("AIR", new Coordinate(2699, 3249, 0)),
            build("AIQ", new Coordinate(2995, 3112, 0)),
            build("AJR", new Coordinate(2779, 3615, 0)),
            build("AJS", new Coordinate(2499, 3898, 0)),
            build("AKP", new Coordinate(3283, 2704, 0)),
            build("AKQ", new Coordinate(2318, 3617, 0)),
            build("AKS", new Coordinate(2570, 2958, 0)),
            build("ALP", new Coordinate(2502, 3638, 0)),
            build("ALQ", new Coordinate(3598, 3496, 0)),
            build("ALS", new Coordinate(2643, 3497, 0)),
            build("BIP", new Coordinate(3409, 3326, 0)),
            build("BIQ", new Coordinate(3248, 3095, 0)),
            build("BIS", new Coordinate(2635, 3268, 0)),
            build("BJP", new Coordinate(2264, 2976, 0)),
            build("BJS", new Coordinate(2147, 3069, 0)),
            build("BKP", new Coordinate(2384, 3037, 0)),
            build("BKR", new Coordinate(3468, 3433, 0)),
            build("BLP", new Coordinate(2432, 5127, 0)),
            build("BLR", new Coordinate(2739, 3353, 0)),
            build("CIP", new Coordinate(2512, 3886, 0)),
            build("CIR", new Coordinate(1303, 3762, 0)),
            build("CIQ", new Coordinate(2527, 3129, 0)),
            build("CJR", new Coordinate(2704, 3578, 0)),
            build("CKR", new Coordinate(2800, 3003, 0)),
            build("CKS", new Coordinate(3446, 3472, 0)),
            build("CLP", new Coordinate(3081, 3208, 0)),
            build("CLS", new Coordinate(2681, 3083, 0)),
            build("DIP", new Coordinate(3039, 4757, 0)),
            build("DIS", new Coordinate(3109, 3149, 0)),
            build("DJP", new Coordinate(2658, 3229, 0)),
            build("DJR", new Coordinate(1452, 3659, 0)),
            build("DKP", new Coordinate(2899, 3113, 0)),
            build("DKR", new Coordinate(3126, 3496, 0)),
            build("DKS", new Coordinate(2743, 3721, 0)),
            build("DLQ", new Coordinate(3422, 3018, 0)),
            build("DLR", new Coordinate(2212, 3101, 0)),
            build("CIS", new Coordinate(1638, 3868, 0)),
            build("CLR", new Coordinate(2737, 2739, 0))
        );
    }

    private FairyRing build(final String code, final Coordinate position) {
        return FairyRing.builder()
            .position(position)
            .code(code)
            .requirement(
                Requirements.and(
                    new QuestRequirement("FAIRYTALE_I_GROWING_PAINS", QuestRequirement.State.COMPLETE),
                    new QuestRequirement("FAIRYTALE_II_CURE_A_QUEEN", QuestRequirement.State.NOT_STARTED).not(),
                    new EquipmentRequirement(Pattern.compile("(?:Dramen|Lunar) staff"))
                )
            )
            .build();
    }
}
