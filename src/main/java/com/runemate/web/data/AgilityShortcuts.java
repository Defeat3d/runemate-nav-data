package com.runemate.web.data;

import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import java.util.*;
import lombok.experimental.*;

/**
 * Basic agility shortcuts
 */
@UtilityClass
public class AgilityShortcuts {

    public Collection<FixedTransport> getAll() {
        return Arrays.asList(
            GameObjectTransport.builder().description("Mos Le'Harmless - Stepping Stone")
                .source(new Coordinate(3708, 2969, 0))
                .destination(new Coordinate(3714, 2969, 0))
                .objectName("Stepping stone")
                .objectAction("Jump-to")
                .objectPosition(new Coordinate(3711, 2969, 0))
                .requirement(SkillRequirement.Skill.AGILITY.required(60))
                .build()
        );
    }

}
