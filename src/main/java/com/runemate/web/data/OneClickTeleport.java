package com.runemate.web.data;

import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import lombok.*;

@Getter
public enum OneClickTeleport {
    // Tabs
    WATERBIRTH_TELEPORT_TAB(new Coordinate(2546, 3757, 0), "Waterbirth teleport", "Break"),
    KHAZARD_TELEPORT_TAB(new Coordinate(2637, 3166, 0), "Khazard teleport", "Break"),
    VARROCK_TELEPORT_TAB(new Coordinate(3212, 3424, 0), "Varrock teleport", "Break"),
    LUMBRIDGE_TELEPORT_TAB(new Coordinate(3225, 3219, 0), "Lumbridge teleport", "Break"),
    FALADOR_TELEPORT_TAB(new Coordinate(2966, 3379, 0), "Falador teleport", "Break"),
    CAMELOT_TELEPORT_TAB(new Coordinate(2757, 3479, 0), "Camelot teleport", "Break"),
    ARDOUGNE_TELEPORT_TAB(new Coordinate(2661, 3300, 0), "Ardougne teleport", "Break"),
    WEST_ARDOUGNE_TELEPORT_TAB(new Coordinate(2500, 3290, 0), "West ardougne teleport", "Break"),
    RIMMINGTON_TELEPORT_TAB(new Coordinate(2954, 3224, 0), "Rimmington teleport", "Break"),
    TAVERLEY_TELEPORT_TAB(new Coordinate(2894, 3465, 0), "Taverley teleport", "Break"),
    RELLEKKA_TELEPORT_TAB(new Coordinate(2668, 3631, 0), "Rellekka teleport", "Break"),
    BRIMHAVEN_TELEPORT_TAB(new Coordinate(2758, 3178, 0), "Brimhaven teleport", "Break"),
    POLLNIVNEACH_TELEPORT_TAB(new Coordinate(3340, 3004, 0), "Pollnivneach teleport", "Break"),
    YANILLE_TELEPORT_TAB(new Coordinate(2544, 3095, 0), "Yanille teleport", "Break"),
    HOSIDIUS_TELEPORT_TAB(new Coordinate(1744, 3517, 0), "Hosidius teleport", "Break"),
    SALVE_GRAVEYARD_TELEPORT_TAB(new Coordinate(3432, 3460, 0), "Salve Graveyard teleport", "Break"),

    // Scrolls
    FELDIP_HILLS_TELEPORT(new Coordinate(2541, 2925, 0), "Feldip hills teleport", "Teleport"),
    PISCATORIS_TELEPORT(new Coordinate(2342, 3647, 0), "Piscatoris teleport", "Teleport"),
    MORTTON_TELEPORT(new Coordinate(3488, 3288, 0), "Mort'ton teleport", "Teleport"),
    DIGSITE_TELEPORT(new Coordinate(3324, 3412, 0), "Digsite teleport", "Teleport"),
    NARDAH_TELEPORT(new Coordinate(3420, 2917, 0), "Nardah teleport", "Teleport"),
    LUMBERYARD_TELEPORT(new Coordinate(3302, 3486, 0), "Lumberyard teleport", "Teleport"),
    TAI_BWO_TELEPORT(new Coordinate(2789, 3066, 0), "Tai bwo wannai teleport", "Teleport"),
    ZULANDRA_TELEPORT(new Coordinate(2197, 3055, 0), "Zul-andra teleport", "Teleport"),
    IORWERTH_TELEPORT(new Coordinate(2194, 3258, 0), "Iorwerth camp teleport", "Teleport"),

    // Items
    ECTOPHIAL(new Coordinate(3659, 3523, 0), "Ectophial", "Empty"),
    ROYAL_SEED_POD(new Coordinate(2465, 3495, 0), "Royal seed pod", "Commune", 30);

    private final Coordinate destination;
    private final String name;
    private final String action;
    private final int maxWildyDepth;
    private final Requirement additionalRequirements;

    OneClickTeleport(
        final Coordinate destination,
        final String name,
        final String action,
        final int maxWildyDepth,
        final Requirement additionalRequirements
    ) {
        this.destination = destination;
        this.name = name;
        this.action = action;
        this.maxWildyDepth = maxWildyDepth;
        this.additionalRequirements = additionalRequirements;
    }

    OneClickTeleport(final Coordinate destination, final String name, final String action) {
        this(destination, name, action, 20, null);
    }

    OneClickTeleport(final Coordinate destination, final String name, final String action, final int maxWildyDepth) {
        this(destination, name, action, maxWildyDepth, null);
    }

    OneClickTeleport(final Coordinate destination, final String name, final String action, final Requirement additionalRequirements) {
        this(destination, name, action, 20, additionalRequirements);
    }
}
