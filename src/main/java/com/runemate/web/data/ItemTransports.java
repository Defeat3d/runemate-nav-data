package com.runemate.web.data;

import com.runemate.web.*;
import com.runemate.web.data.model.*;
import com.runemate.web.requirement.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;

/**
 * Defines Item Teleports
 */
@UtilityClass
public class ItemTransports {

    @Getter(lazy = true) private final Collection<BasicItemTransport> basic = loadBasicItemTransports();

    @Getter(lazy = true) private final Collection<ChatOptionItemTransport> dialog = loadDialogItemTransports();

    public List<ChatOptionItemTransport> getDialogItems(@NonNull WebContext context) {
        return getDialog().stream().filter(it -> it.getRequirement().satisfy(context)).collect(Collectors.toList());
    }

    public List<BasicItemTransport> getBasicItems(@NonNull WebContext context) {
        return getBasic().stream().filter(it -> it.getRequirement().satisfy(context)).collect(Collectors.toList());
    }

    public List<DynamicTransport> getAll(@NonNull WebContext context) {
        final var result = new ArrayList<DynamicTransport>();
        result.addAll(getBasicItems(context));
        result.addAll(getDialogItems(context));
        return result;
    }

    private List<BasicItemTransport> loadBasicItemTransports() {
        final List<BasicItemTransport> results = new ArrayList<>();

        for (final var teleport : OneClickTeleport.values()) {
            final var name = Pattern.compile(teleport.getName());
            var requirement = Requirements.and(new ItemRequirement(name, 1), new WildernessRequirement(teleport.getMaxWildyDepth()));
            if (teleport.getAdditionalRequirements() != null) {
                requirement = requirement.and(teleport.getAdditionalRequirements());
            }
            final var transport = BasicItemTransport.builder()
                .description(teleport.getName())
                .itemName(name)
                .itemAction(Pattern.compile(teleport.getAction()))
                .destination(teleport.getDestination())
                .origin(BasicItemTransport.Origin.INVENTORY).requirement(requirement)
                .build();
            results.add(transport);
        }

        //When worn most jewelry can teleport directly through right-click
        for (final var tele : Jewelry.values()) {
            for (final var dest : tele.getDestinations()) {
                var requirement = Requirements.and(
                    new EquipmentRequirement(tele.getName()),
                    new WildernessRequirement(tele.getMaxWildyDepth())
                );
                var transport = BasicItemTransport.builder()
                    .description(tele.getName().pattern())
                    .itemName(tele.getName())
                    .itemAction(dest.getName())
                    .destination(dest.getPosition())
                    .origin(BasicItemTransport.Origin.EQUIPMENT)
                    .requirement(requirement)
                    .build();
                results.add(transport);
            }
        }

        return results;
    }

    private List<ChatOptionItemTransport> loadDialogItemTransports() {
        final List<ChatOptionItemTransport> results = new ArrayList<>();

        //When in inventory, "Rub" > select dialog option
        for (final var teleport : Jewelry.values()) {
            for (final var dest : teleport.getDestinations()) {
                var transport = ChatOptionItemTransport.builder()
                    .description(dest.getName().pattern())
                    .destination(dest.getPosition())
                    .itemName(teleport.getName())
                    .itemAction(teleport.getAction())
                    .optionPattern(dest.getName())
                    .origin(BasicItemTransport.Origin.INVENTORY)
                    .requirement(Requirements.and(
                        new ItemRequirement(teleport.getName(), 1),
                        new WildernessRequirement(teleport.getMaxWildyDepth())
                    ))
                    .build();
                results.add(transport);
            }
        }

        return results;
    }
}
