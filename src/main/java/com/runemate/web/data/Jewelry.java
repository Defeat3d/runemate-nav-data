package com.runemate.web.data;

import com.runemate.web.data.model.*;
import java.util.regex.*;
import lombok.*;

@Getter
public enum Jewelry {
    RING_OF_DUELING("Ring of dueling", 20, new Destination[] {
        new Destination("PvP Arena", new Coordinate(3315, 3235, 0)),
        new Destination("Castle Wars", new Coordinate(2440, 3090, 0)),
        new Destination("Ferox Enclave", new Coordinate(3151, 3635, 0))
    }),
    GAMES_NECKLACE("Games necklace", 20, new Destination[] {
        new Destination("Burthorpe", new Coordinate(2898, 3553, 0)),
        new Destination("Barbarian Output", new Coordinate(2520, 3571, 0)),
        new Destination("Corporeal Beast", new Coordinate(2964, 4382, 2)),
        new Destination("Tears of Guthix", new Coordinate(3244, 9501, 2)),
        new Destination("Wintertodt Camp", new Coordinate(1624, 3938, 0))
    }),
    NECKLACE_OF_PASSAGE("Necklace of passage", 20, new Destination[] {
        new Destination("Wizards' Tower", new Coordinate(3114, 3179, 0)),
        new Destination("The Output", new Coordinate(2430, 3348, 0)),
        new Destination("Eagles' Eyrie", new Coordinate(3405, 3157, 0))
    }),
    XERICS_TALISMAN("Xeric's talisman", 20, new Destination[] {
        new Destination("Xeric's Lookout", new Coordinate(1576, 3530, 0)),
        new Destination("Xeric's Glade", new Coordinate(1752, 3566, 0)),
        new Destination("Xeric's Heart", new Coordinate(1640, 3674, 0)),
        new Destination("Xeric's Inferno", new Coordinate(1504, 3817, 0))
    }),
    SLAYER_RING("Slayer ring", 30, Pattern.compile("(Rub|Teleport)"), new Destination[] {
        new Destination(Pattern.compile("(Teleport|Stronghold Slayer Cave)"), new Coordinate(2432, 3423, 0)),
        new Destination(Pattern.compile("(Teleport|Slayer Tower)"), new Coordinate(3422, 3537, 0)),
        new Destination(Pattern.compile("(Teleport|Fremennik Slayer Dungeon)"), new Coordinate(2802, 10000, 0)),
        new Destination(Pattern.compile("(Teleport|Tarn's Lair)"), new Coordinate(3185, 4601, 0)),
        new Destination(Pattern.compile("(Teleport|Dark Beasts)"), new Coordinate(2028, 4636, 0))
    }),
    DIGSITE_PENDANT("Digsite pendant", 20, new Destination[] {
        new Destination("Digsite", new Coordinate(3341, 3445, 0)),
        new Destination("Fossil Island", new Coordinate(3764, 3869, 1)),
        new Destination("Lithkren", new Coordinate(3549, 10456, 0))
    }),
    COMBAT_BRACELET("Combat bracelet", 30, new Destination[] {
        new Destination("Warriors' Guild", new Coordinate(2882, 3548, 0)),
        new Destination("Champions' Guild", new Coordinate(3191, 3367, 0)),
        new Destination("Monastery", new Coordinate(3052, 3488, 0)),
        new Destination("Ranging Guild", new Coordinate(2655, 3441, 0))
    }),
    SKILLS_NECKLACE("Skills necklace", 30, new Destination[] {
        new Destination("Fishing Guild", new Coordinate(2611, 3390, 0)),
        new Destination("Mining Guild", new Coordinate(3050, 9763, 0)),
        new Destination("Crafting Guild", new Coordinate(2933, 3295, 0)),
        new Destination("Cooking Guild", new Coordinate(3143, 3440, 0)),
        new Destination("Woodcutting Guild", new Coordinate(1662, 3505, 0)),
        new Destination("Farming Guild", new Coordinate(1249, 3718, 0)),
    }),
    RING_OF_WEALTH("Ring of wealth", 30, new Destination[] {
        new Destination("Grand Exchange", new Coordinate(3163, 3478, 0)),
        new Destination("Falador", new Coordinate(2996, 3375, 0)),
    }),
    AMULET_OF_GLORY("Amulet of glory", 30, new Destination[] {
        new Destination("Edgeville", new Coordinate(3087, 3496, 0)),
        new Destination("Karamja", new Coordinate(2918, 3176, 0)),
        new Destination("Draynor Village", new Coordinate(3105, 3251, 0)),
        new Destination("Al Kharid", new Coordinate(3293, 3163, 0)),
    });

    private final Pattern name;
    private final int maxWildyDepth;
    private final Destination[] destinations;
    private final Pattern action;

    Jewelry(final Pattern name, int maxWildyDepth, final Destination[] destinations) {
        this.name = name;
        this.action = Pattern.compile("Rub");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
    }

    Jewelry(final String name, int maxWildyDepth, final Destination[] destinations) {
        this.name = Pattern.compile(name + " ?\\(\\d+\\)");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
        this.action = Pattern.compile("Rub");
    }

    Jewelry(final String name, int maxWildyDepth, final Pattern action, final Destination[] destinations) {
        this.name = Pattern.compile(name + " ?\\(\\d+\\)");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
        this.action = action;
    }

    @Getter
    @Value
    @AllArgsConstructor
    public static class Destination {

        Pattern name;
        Coordinate position;

        public Destination(final String name, final Coordinate position) {
            this.name = Pattern.compile(name);
            this.position = position;
        }
    }
}
