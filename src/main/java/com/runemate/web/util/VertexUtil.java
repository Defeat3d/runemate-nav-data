package com.runemate.web.util;

import com.runemate.web.data.model.*;
import com.runemate.web.edge.*;
import com.runemate.web.vertex.*;
import lombok.*;
import lombok.experimental.*;

@UtilityClass
public class VertexUtil {

    private final Area WILDERNESS_1 = new Area(2944, 3520, 447, 831);
    private final Area WILDERNESS_2 = new Area(2944, 3520, 63, 63);
    private final Area WILDERNESS_3 = new Area(2944, 3520, 511, 959);
    private final Area VARROCK_DARK_CIRCLE = new Area(3216, 3361, 20, 20);

    public boolean isInWilderness(@NonNull Edge<Vertex> edge) {
        return isInWilderness(edge.origin()) || isInWilderness(edge.destination());
    }

    public boolean isInWilderness(@NonNull Vertex vertex) {
        return getWildernessLevel(vertex) > 0;
    }

    /**
     * Ported from the logic provided in [proc,wilderness_level]()(int)
     * <p>
     * [proc,wilderness_level]()(int)
     * def_int $int0 = 0;
     * if (~inzone(0_46_55_0_0, 3_52_67_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 55 * 64) / 8 + 1);
     * } else if (~inzone(0_47_158_0_0, 3_47_158_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 155 * 64) / 8 - 1);
     * } else if (~inzone(0_46_155_0_0, 3_53_169_63_63, coord) = 1) {
     * $int0 = calc((coordz(coord) - 155 * 64) / 8 + 1);
     * }
     * return($int0);
     */
    public static int getWildernessLevel(Vertex position) {
        int level = 0;
        if (WILDERNESS_1.contains(position)) {
            level = (position.y() - 55 * 64) / 8 + 1;
        } else if (WILDERNESS_2.contains(position)) {
            level = (position.y() - 155 * 64) / 8 - 1;
        } else if (WILDERNESS_3.contains(position)) {
            level = (position.y() - 155 * 64) / 8 + 1;
        }
        return level;
    }

    public static int getWildernessLevel(Edge<Vertex> edge) {
        return Math.max(getWildernessLevel(edge.origin()), getWildernessLevel(edge.destination()));
    }

    public static boolean areaContains(Vertex minimum, Vertex maximum, Vertex current) {
        if (current.x() < minimum.x() || current.x() > maximum.x()) {
            return false;
        }
        if (current.y() < minimum.y() || current.y() > maximum.y()) {
            return false;
        }
        return current.z() >= minimum.z() && current.z() <= maximum.z();
    }

    public static float calculateCostBetween(Vertex source, Vertex target) {
        float cost = (float) distance(source, target);
        int wildernessLevel = getWildernessLevel(target);
        if (wildernessLevel > 0) {
            cost *= 3;
            cost += wildernessLevel * 0.4;
        }
        if (VARROCK_DARK_CIRCLE.contains(source) || VARROCK_DARK_CIRCLE.contains(target)) {
            cost *= 3;
        }
        return cost;
    }

    public static double distance(Vertex first, Vertex second) {
        return calculate(first.x(), first.y(), second.x(), second.y());
    }

    public static double calculate(long startX, long startY, long destinationX, long destinationY) {
        if (startX == destinationX && startY == destinationY) {
            return 0;
        }
        return Math.addExact(
            Math.abs(Math.subtractExact(destinationX, startX)),
            Math.abs(Math.subtractExact(destinationY, startY))
        );
    }
}
