package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class VarpRequirement implements Requirement {

    int varpIndex;
    int varpValue;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasVarp(varpIndex, varpValue);
    }

    @Override
    public int type() {
        return Type.VARP;
    }
}
