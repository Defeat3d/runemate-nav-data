package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class QuestRequirement implements Requirement {

    String quest;
    State state;

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return context.hasQuestState(quest, state);
    }

    @Override
    public int type() {
        return Type.QUEST_STATE;
    }

    public enum State {
        NOT_STARTED,
        IN_PROGRESS,
        COMPLETE,
        UNKNOWN
    }
}
