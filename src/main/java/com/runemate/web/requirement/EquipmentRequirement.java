package com.runemate.web.requirement;

import com.runemate.web.*;
import java.util.regex.*;
import lombok.*;

@Value
public class EquipmentRequirement implements Requirement {

    @NonNull Pattern itemName;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.isEquipped(itemName);
    }

    @Override
    public int type() {
        return Type.EQUIPMENT;
    }
}
