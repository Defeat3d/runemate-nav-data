package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class VarbitRequirement implements Requirement {

    int varbitIndex;
    int varbitValue;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasVarbit(varbitIndex, varbitValue);
    }

    @Override
    public int type() {
        return Type.VARP;
    }
}
