package com.runemate.web.requirement;

import com.runemate.web.*;
import com.runemate.web.util.*;
import com.runemate.web.vertex.*;
import lombok.*;

@Value
public class WildernessRequirement implements Requirement {

    int minLevel;
    int maxLevel;

    public WildernessRequirement(int max) {
        this(0, max);
    }

    public WildernessRequirement(int min, int max) {
        minLevel = min;
        maxLevel = max;
    }

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return true; //context.isWithinWildernessDepth(minLevel, maxLevel);
    }

    @Override
    public int type() {
        return Type.WILDERNESS;
    }

    public static WildernessRequirement of(Vertex v1, Vertex v2) {
        return new WildernessRequirement(0, Math.max(VertexUtil.getWildernessLevel(v1), VertexUtil.getWildernessLevel(v2)));
    }
}
