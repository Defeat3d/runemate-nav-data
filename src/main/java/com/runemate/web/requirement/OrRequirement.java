package com.runemate.web.requirement;

import com.runemate.web.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;
import lombok.experimental.*;
import org.jetbrains.annotations.*;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EqualsAndHashCode
@ToString
public final class OrRequirement implements Requirement {

    @NotNull @Getter Set<Requirement> requirements;

    public OrRequirement(@NonNull Stream<Requirement> requirements) {
        this(requirements.collect(Collectors.toSet()));
    }

    public OrRequirement(@NonNull Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return requirements.stream().anyMatch(r -> r.satisfy(context));
    }

    @Override
    public int type() {
        return Type.OR;
    }
}
