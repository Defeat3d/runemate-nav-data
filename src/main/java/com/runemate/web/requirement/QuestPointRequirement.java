package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class QuestPointRequirement implements Requirement {

    int points;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasQuestPoints(points);
    }

    @Override
    public int type() {
        return Type.QUEST_POINTS;
    }
}
