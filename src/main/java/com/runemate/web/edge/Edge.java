package com.runemate.web.edge;

import com.runemate.web.requirement.*;
import org.jetbrains.annotations.*;

public /*sealed*/ interface Edge<V> /*permits EdgeBase*/ {

    @NotNull V origin();

    @NotNull V destination();

    float cost();

    @NotNull Requirement requirement();

    int type();

    interface Type {

        int LITERAL = 1;
        int OBJECT = 2;
        int CHAT_OBJECT = 3;
        int ITEM = 4;
        int SPIRIT_TREE = 5;
        int FAIRY_RING = 6;
        int NPC = 7;
        int CHAT_NPC = 8;
    }
}
