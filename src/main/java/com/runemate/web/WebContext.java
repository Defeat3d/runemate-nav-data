package com.runemate.web;

import com.runemate.web.requirement.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;

@Value
@Builder(toBuilder = true)
public class WebContext {

    @Builder.Default Map<String, Integer> inventory = Collections.emptyMap();
    @Builder.Default List<String> equipment = Collections.emptyList();
    @Builder.Default Map<SkillRequirement.Skill, Integer> skills = Collections.emptyMap();
    @Builder.Default Map<Integer, Integer> varps = Collections.emptyMap();
    @Builder.Default Map<Integer, Integer> varbits = Collections.emptyMap();
    @Builder.Default Map<String, QuestRequirement.State> quests = Collections.emptyMap();

    int questPoints;
    boolean member;
    int gold;
    int wildernessLevel;

    public static WebContext empty() {
        return new WebContextBuilder().build();
    }

    public boolean hasQuestState(String quest, QuestRequirement.State state) {
        return state != QuestRequirement.State.UNKNOWN && quests.entrySet()
            .stream().anyMatch(e -> e.getKey().equalsIgnoreCase(quest) && Objects.equals(state, e.getValue()));
    }

    public boolean hasInventoryItem(Pattern name, int quantity) {
        return inventory.entrySet()
            .stream().anyMatch(e -> name.matcher(e.getKey()).matches() && e.getValue() >= quantity);
    }

    public boolean isMember() {
        return member;
    }

    public boolean isWithinWildernessDepth(int min, int max) {
        return wildernessLevel >= min && wildernessLevel <= max;
    }

    public boolean isEquipped(Pattern name) {
        return equipment.stream().anyMatch(s -> name.matcher(s).matches());
    }

    public boolean hasSkillLevel(SkillRequirement.Skill skill, int level) {
        return skills.getOrDefault(skill, 0) >= level;
    }

    public boolean hasVarp(int varp, int value) {
        return varps.getOrDefault(varp, Integer.MIN_VALUE) == value;
    }

    public boolean hasVarbit(int varbit, int value) {
        return varbits.getOrDefault(varbit, Integer.MIN_VALUE) == value;
    }

    public boolean hasQuestPoints(int points) {
        return points <= questPoints;
    }

    public boolean hasGold(int quantity) {
        return quantity <= gold;
    }
}
