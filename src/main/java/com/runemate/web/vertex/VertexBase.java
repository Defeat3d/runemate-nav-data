package com.runemate.web.vertex;

import java.util.*;

public abstract class VertexBase implements Vertex {

    @Override
    public /*final*/ boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vertex)) {
            return false;
        }
        Vertex that = (Vertex) o;
        return x() == that.x() && y() == that.y() && z() == that.z();
    }

    @Override
    public /*final*/ int hashCode() {
        return Objects.hash(x(), y(), z());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Vertex.class.getSimpleName() + "[", "]")
            .add("x=" + x())
            .add("y=" + y())
            .add("z=" + z())
            .toString();
    }
}
