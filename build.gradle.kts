plugins {
    java
    idea
    `java-library`
    `maven-publish`
    kotlin("jvm") version "1.6.10"
    id("io.freefair.lombok") version "6.3.0"
}

group = "com.runemate"
version = "1.1.1-SNAPSHOT"

val externalRepositoryUrl: String by project
val externalRepositoryPrivateToken: String? by project

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {

}

tasks {
    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
        }
    }
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }
    kotlin {
        jvmToolchain {
            (this as JavaToolchainSpec).languageVersion.set(JavaLanguageVersion.of(11))
        }
    }
}


publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }
    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryPrivateToken != null) {
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = externalRepositoryPrivateToken
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}